package com.example.contacts.datamodel.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.contacts.datamodel.local.db.AppDatabase
import com.example.contacts.datamodel.local.db.dao.ContactDAO
import com.example.contacts.datamodel.model.Contact

class ContactRepository(context: Context) {
    private var mContactDAO: ContactDAO

    init {
        val db: AppDatabase = AppDatabase.invoke(context)
        mContactDAO = db.contactDAO()
    }

    fun deleteAll() {
        mContactDAO.deleteAll()
    }

    fun insert(contacts: List<Contact?>) {
        mContactDAO.insert(contacts)
    }

    fun fetchAll(): LiveData<List<Contact>> {
        return mContactDAO.fetchAll()
    }

    fun fetchById(id: Long): LiveData<Contact> {
        return mContactDAO.fetchById(id)
    }


}