package com.example.contacts.datamodel.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.contacts.datamodel.model.Contact

@Dao
interface ContactDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contacts: List<Contact?>?)

    @Query("DELETE FROM contact")
    fun deleteAll()

    @Query("SELECT * FROM contact")
    fun fetchAll(): LiveData<List<Contact>>

    @Query("SELECT * FROM contact WHERE id = :id")
    fun fetchById(id: Long): LiveData<Contact>
}