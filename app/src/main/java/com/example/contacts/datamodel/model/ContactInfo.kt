package com.example.contacts.datamodel.model

import android.os.Parcel
import android.os.Parcelable

data class ContactsInfo (
    var contactId: Long? = null,
    var displayName: String? = null,
    var phoneNumber: Array<String>? = null,
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.createStringArray()
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ContactsInfo

        if (contactId != other.contactId) return false
        if (displayName != other.displayName) return false
        if (phoneNumber != null) {
            if (other.phoneNumber == null) return false
            if (!phoneNumber.contentEquals(other.phoneNumber)) return false
        } else if (other.phoneNumber != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = contactId?.hashCode() ?: 0
        result = 31 * result + (displayName?.hashCode() ?: 0)
        result = 31 * result + (phoneNumber?.contentHashCode() ?: 0)
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(contactId)
        parcel.writeString(displayName)
        parcel.writeStringArray(phoneNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactsInfo> {
        override fun createFromParcel(parcel: Parcel): ContactsInfo {
            return ContactsInfo(parcel)
        }

        override fun newArray(size: Int): Array<ContactsInfo?> {
            return arrayOfNulls(size)
        }
    }
}
