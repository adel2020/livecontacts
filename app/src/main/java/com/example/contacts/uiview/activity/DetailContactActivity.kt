@file:Suppress("DEPRECATION")

package com.example.contacts.uiview.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.contacts.R
import com.example.contacts.databinding.DetailContactActivityBinding
import com.example.contacts.viewmodel.DetailContactVM
import com.example.contacts.viewmodel.factory.DetailContactVMFactory

@Suppress("DEPRECATION")
class DetailContactActivity : AppCompatActivity() {
    private var factory: DetailContactVMFactory? = null
    private var detailContactVM: DetailContactVM? = null
    private var binding: DetailContactActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =DataBindingUtil.setContentView(this,R.layout.detail_contact_activity)
        factory = DetailContactVMFactory(intent.getParcelableExtra("contact")!!)
        detailContactVM = ViewModelProviders.of(this, factory).get(DetailContactVM::class.java)
        binding?.detailContact = detailContactVM
    }
}