@file:Suppress("DEPRECATION")

package com.example.contacts.uiview.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contacts.R
import com.example.contacts.datamodel.model.Contact
import com.example.contacts.uiview.adapter.ContactListAdapter
import com.example.contacts.uiview.listener.RecyclerViewClickListener
import com.example.contacts.viewmodel.factory.ContactVMFactory
import com.example.contacts.viewmodel.GetContactVM


class MainActivity : AppCompatActivity(), RecyclerViewClickListener {

    private val PERMISSIONS_REQUEST_READ_CONTACTS = 1
    var dataAdapter: ContactListAdapter? = null
    var rvContact: RecyclerView? = null
    var getContactVM: GetContactVM? = null
    private var factory: ContactVMFactory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        factory = ContactVMFactory(this)
        getContactVM = ViewModelProviders.of(this, factory).get(GetContactVM::class.java)

        rvContact = findViewById(R.id.rv_contact)
        rvContact?.layoutManager = LinearLayoutManager(this)
        rvContact?.setHasFixedSize(true)
        requestContactPermission()

        getContactVM!!.getContacts().observe(
            this
        ) { contacts ->
            dataAdapter = ContactListAdapter(contacts as ArrayList<Contact>,this)
            rvContact?.adapter = dataAdapter
        }
    }

    private fun requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_CONTACTS
                    )
                ) {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Read contacts access needed")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setMessage("Please enable access to contacts.")
                    builder.setOnDismissListener {
                        requestPermissions(
                            arrayOf(Manifest.permission.READ_CONTACTS),
                            PERMISSIONS_REQUEST_READ_CONTACTS
                        )
                    }
                    builder.show()
                } else {
                    requestPermissions(
                        this, arrayOf(Manifest.permission.READ_CONTACTS),
                        PERMISSIONS_REQUEST_READ_CONTACTS
                    )
                }
            } else {
                getContactVM!!.startObs()
            }
        } else {
            getContactVM!!.startObs()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_READ_CONTACTS -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    getContactVM!!.startObs()

                } else {
                    Toast.makeText(
                        this,
                        "You have disabled a contacts permission",
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
        }
    }

    override fun onRecyclerViewItemClick(view: View?, listItem: Any?, position: Int) {
        Intent(this, DetailContactActivity::class.java).also {
            it.putExtra("contact", (listItem as Contact))
            startActivity(it)
        }
    }


}