package com.example.contacts.uiview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.contacts.R
import com.example.contacts.databinding.ContactInfoModelBinding
import com.example.contacts.datamodel.model.Contact
import com.example.contacts.uiview.activity.MainActivity

class ContactListAdapter(
    private val contactList: ArrayList<Contact>,
    context: Context
) : RecyclerView.Adapter<ContactListAdapter.ContactListViewHolder>() {
    private val mContext: Context = context

    inner class ContactListViewHolder(
        val modelListContact: ContactInfoModelBinding
    ) : RecyclerView.ViewHolder(modelListContact.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactListViewHolder =
        ContactListViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.contact_info_model,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ContactListViewHolder, position: Int) {
        holder.modelListContact.contact = contactList[position]
        holder.itemView.setOnClickListener {
            (mContext as MainActivity).onRecyclerViewItemClick(
                holder.itemView,
                contactList[position],
                position
            )
        }
    }

    override fun getItemCount(): Int = contactList.size


}
