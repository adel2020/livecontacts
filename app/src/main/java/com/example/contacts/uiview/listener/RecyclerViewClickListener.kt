package com.example.contacts.uiview.listener

import android.view.View

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View?, listItem: Any?, position: Int)
}