package com.example.contacts.viewmodel

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Handler
import android.provider.ContactsContract
import androidx.annotation.RequiresApi
import androidx.annotation.WorkerThread
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.contacts.datamodel.model.Contact
import com.example.contacts.datamodel.model.ContactsInfo
import com.example.contacts.datamodel.repository.ContactRepository
import com.example.contacts.viewmodel.sync.ContactObserver
import com.example.contacts.viewmodel.sync.ContactIntentService
import kotlin.collections.ArrayList


class GetContactVM(context: Context) : ViewModel() {
    private val mContext: Context = context
    private var mContactObserver: ContactObserver? = null
    private var mRepository: ContactRepository? = null
    private var mContacts: LiveData<List<Contact>>? = null


    init {
        mRepository = ContactRepository(context)
        mContactObserver = ContactObserver(Handler(), context)
    }

    fun getContacts(): LiveData<List<Contact>> {
        if (mContacts == null) {
            mContacts = mRepository!!.fetchAll()
        }
        return mContacts as LiveData<List<Contact>>
    }

    fun startObs() {
        mContactObserver?.let {
            mContext.contentResolver.registerContentObserver(
                ContactsContract.Contacts.CONTENT_URI,
                true,
                it
            )
        }
        ContactIntentService.enqueueWork(mContext, Intent())
    }

    companion object {
        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @WorkerThread
        @JvmStatic
        fun matchContent(context: Context?) {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    android.Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) return
            val pContacts: List<ContactsInfo>? = getContacts(context)
            if (pContacts != null) {
                val repository = ContactRepository(context)
                val contacts: ArrayList<Contact> = ArrayList()
                repository.deleteAll()

                for (j in pContacts.indices) {
                    val mContact = Contact()
                    mContact.id = pContacts[j].contactId
                    mContact.name = pContacts[j].displayName
                    pContacts[j].phoneNumber?.forEach {
                        mContact.phone = "$it "
                    }
                    contacts.add(mContact)
                }
                repository.insert(contacts)
            }
        }

        @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        private fun getContacts(context: Context): List<ContactsInfo> {
            var contactId: String?
            var displayName: String?
            var contactsInfoList: List<ContactsInfo>? = null
            val cursor: Cursor? = context.contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
            )
            if (cursor?.count!! > 0) {
                contactsInfoList = ArrayList(cursor.count)
                while (cursor.moveToNext()) {
                    val hasPhoneNumber: Int =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                            .toInt()
                    if (hasPhoneNumber > 0) {
                        contactId =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                        displayName =
                            cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                        val phoneCursor: Cursor? = context.contentResolver.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf<String?>(
                                contactId
                            ),
                            null
                        )

                        val contactsInfo = ContactsInfo(
                            java.lang.Long.valueOf(contactId),
                            displayName,
                            null,
                        )
                        if (phoneCursor != null) {
                            val pCurCount: Int = phoneCursor.count
                            val phones: MutableList<String> = ArrayList(pCurCount)
                            while (phoneCursor.moveToNext()) {
                                var contactNumber: String =
                                    phoneCursor.getString(
                                        phoneCursor.getColumnIndex(
                                            ContactsContract.CommonDataKinds.Phone.NUMBER
                                        )
                                    )
                                if (contactNumber.isNotEmpty()) {
                                    contactNumber = contactNumber.replace(" ", "")
                                    phones.add(contactNumber)
                                }
                            }
                            phoneCursor.close()
                            contactsInfo.phoneNumber = phones.toTypedArray()
                        }
                        contactsInfoList.add(contactsInfo)
                    }
                }
            }
            cursor.close()
            return contactsInfoList!!
        }
    }


}