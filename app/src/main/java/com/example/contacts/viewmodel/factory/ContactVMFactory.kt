package com.example.contacts.viewmodel.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.contacts.viewmodel.GetContactVM

@Suppress("UNCHECKED_CAST")
class ContactVMFactory(context: Context): ViewModelProvider.NewInstanceFactory() {
    private val vm = GetContactVM(context)
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return vm as T
    }
}