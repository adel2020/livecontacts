package com.example.contacts.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.contacts.datamodel.model.Contact


class DetailContactVM(detailContact: Contact) : ViewModel() {
    var id: MutableLiveData<String> = MutableLiveData()
    var name: MutableLiveData<String> = MutableLiveData()
    var phoneNumber: MutableLiveData<String> = MutableLiveData()

    init {
        id.value = detailContact.id.toString()
        name.value = detailContact.name.toString()
        phoneNumber.value = detailContact.phone.toString()
    }
}