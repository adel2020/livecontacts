package com.example.contacts.viewmodel.sync

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.JobIntentService
import com.example.contacts.viewmodel.GetContactVM.Companion.matchContent

class ContactIntentService : JobIntentService() {
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @SuppressLint("MissingPermission")
    override fun onHandleWork(intent: Intent) {
        matchContent(this)
    }

    companion object {
        private const val JOB_ID = 0X21
        fun enqueueWork(context: Context?, work: Intent?) {
            enqueueWork(context!!, ContactIntentService::class.java, JOB_ID, work!!)
        }
    }
}