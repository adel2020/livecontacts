package com.example.contacts.viewmodel.sync

import android.content.Context
import android.content.Intent
import android.database.ContentObserver
import android.net.Uri
import android.os.Handler

class ContactObserver : ContentObserver {
    private var context: Context? = null

    internal constructor(handler: Handler?) : super(handler) {}
    constructor(handler: Handler?, context: Context?) : super(handler) {
        this.context = context
    }

    override fun onChange(selfChange: Boolean, uri: Uri) {
        super.onChange(selfChange, uri)
        if (!selfChange) {
            trySync(context)
        }
    }

    fun trySync(context: Context?) {
        ContactIntentService.enqueueWork(context, Intent())
    }
}